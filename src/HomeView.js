import React from 'react'
import { Text, View, StyleSheet, Image, ScrollView } from 'react-native'
import NavTop from './componets/NavTop'
import LineTop from './componets/LineTop'
import CoverPage from './componets/CoverPage'
import NavBotton from './componets/NavBotton'
import Information from './componets/Information'
import Friends from './componets/Friends'
import Publications from './componets/Publications'
import Car from './componets/Car'

const HomeView = props => (
  <View style={styles.container}>
    <View>
      <NavTop />
    </View>
    <ScrollView>
      <View>
        <LineTop />
      </View>
      <View>
        <CoverPage />
      </View>
      <View style={{ marginLeft: 15, marginRight: 15 }}>
        <LineTop />
      </View>
      <View>
        <Information />
      </View>
      <View>
        <LineTop />
      </View>
      <View>
        <Friends />
      </View>
      <View style={styles.pubc}>
        <Publications />
      </View>
      <View>
        <Car />
      </View>
    </ScrollView>
    <View>
      <LineTop />
    </View>
    <View style={styles.NavBotton}>
      <View>
        <LineTop />
      </View>
      <NavBotton />
    </View>
  </View>
)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    backgroundColor: '#fff',
  },
  NavBotton: {
    backgroundColor: '#fff',
  },
  pubc: {},
})

export default HomeView
