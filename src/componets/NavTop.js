import React from 'react'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import { Text, View, StyleSheet, TextInput } from 'react-native'

const NavTop = props => (
  <View style={styles.container}>
    <View style={styles.searchSection}>
      <FontAwesome5 style={styles.backIcon} name={'chevron-left'} size={25} />
      <FontAwesome5
        style={styles.searchIcon}
        name={'search'}
        size={15}
        color="gray"
      />
      <TextInput style={styles.input} placeholder="Name" />
    </View>
  </View>
)

const styles = StyleSheet.create({
  container: {
    height: 60,
    backgroundColor: '#fff',
  },
  searchSection: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  searchIcon: {
    marginLeft: 15,
    padding: 8,
    height: 35,
    backgroundColor: '#e2e2e2',
    borderTopLeftRadius: 24,
    borderBottomLeftRadius: 24,
  },
  backIcon: {
    padding: 10,
    marginLeft: 5,
    color: 'black'
  },
  input: {
    flex: 1,
    height: 35,
    backgroundColor: '#e2e2e2',
    fontSize: 13,
    color: 'black',
    borderTopRightRadius: 24,
    borderBottomRightRadius: 24,
    marginRight: 10,
  },
})

export default NavTop
