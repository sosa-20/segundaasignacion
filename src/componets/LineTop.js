import React from 'react'
import { Text, View, StyleSheet } from 'react-native'

const LineTop = props => (
  <View>
    <View style={styles.lineStyle} />
  </View>
)

const styles = StyleSheet.create({
  lineStyle: {
    borderWidth: 0.5,
    borderColor: '#eff0f1',
  },
})

export default LineTop
