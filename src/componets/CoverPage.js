import React from 'react'
import {
  Text,
  View,
  StyleSheet,
  Image,
  ImageBackground,
  Button,
  TouchableOpacity,
} from 'react-native'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'

const CoverPage = props => (
  <View>
    <View style={styles.container}>
      <ImageBackground
        imageStyle={{ borderTopLeftRadius: 20, borderTopRightRadius: 20 }}
        source={{
          uri:
            'https://previews.123rf.com/images/lighthunter/lighthunter1403/lighthunter140300050/31324708-feliz-grupo-de-personas-familia-y-el-m%C3%A9dico-que-muestra-thumbsup-para-los-servicios-de-alta-calidad-en.jpg',
        }}
        style={styles.image}
      >
        <Image
          source={{
            uri:
              'https://i.pinimg.com/originals/5d/42/c9/5d42c92e565fe88644caf6a4b2bfa30a.jpg',
          }}
          style={styles.imagePerfil}
        />
      </ImageBackground>
    </View>
    <View style={styles.info}>
      <Text style={{ fontWeight: 'bold', fontSize: 25,color: 'black' }}>Isabel Zambrano</Text>
      <Text style={{ fontSize: 18, color: 'gray' }}>
        tranquila, alegre y divertida
      </Text>
    </View>
    <View style={styles.grupButton}>
      <View style={styles.buttonPrincipal}>
        <TouchableOpacity>
          <Text
            style={{
              textAlign: 'center',
              color: '#fff',
              fontSize: 15,
              paddingTop: 10,
              height: 40,
            }}
          >
            <FontAwesome5
              style={{ marginLeft: 20 }}
              name={'user-plus'}
              size={15}
              color="white"
            />
            Agregar Amigos
          </Text>
        </TouchableOpacity>
      </View>
      <View style={styles.buttonSeting}>
        <TouchableOpacity>
          <Text
            style={{
              textAlign: 'center',
              color: '#fff',
              fontSize: 15,
              paddingTop: 10,
              height: 40,
            }}
          >
            <FontAwesome5
              style={{ marginLeft: 20 }}
              name={'cog'}
              size={20}
              color="black"
            />
          </Text>
        </TouchableOpacity>
      </View>
      <View style={styles.buttonOptions}>
        <TouchableOpacity>
          <Text
            style={{
              textAlign: 'center',
              color: '#fff',
              fontSize: 15,
              paddingTop: 10,
              height: 40,
            }}
          >
            <FontAwesome5
              style={{ marginLeft: 20 }}
              name={'ellipsis-h'}
              size={20}
              color="black"
            />
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  </View>
)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: 'absolute',
    height: 300,
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    margin: 15,
  },
  image: {
    flex: 1,
    resizeMode: 'cover',
    height: 200,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  imagePerfil: {
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: 100,
    borderWidth: 7,
    borderColor: '#fff',
    flex: 1,
    resizeMode: 'cover',
    width: 200,
    height: 200,
    borderRadius: 200,
  },
  info: {
    flex: 1,
    marginTop: 320,
    alignItems: 'center',
    justifyContent: 'center',
  },
  grupButton: {
    marginTop: 20,
    margin: 10,
    flexDirection: 'row',
  },
  buttonPrincipal: {
    flex: 1,
    borderRadius: 10,
    backgroundColor: '#1393f4',
    marginRight: 12,
  },
  buttonSeting: {
    paddingLeft: 12,
    paddingRight: 12,
    backgroundColor: '#f7f7f7',
    borderRadius: 10,
    marginRight: 12,
  },
  buttonOptions: {
    paddingLeft: 12,
    paddingRight: 12,
    backgroundColor: '#f7f7f7',
    borderRadius: 10,
  },
})

export default CoverPage
