import React from 'react'
import { Text, View, StyleSheet, Image } from 'react-native'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'

const Car = props => (
  <View>
    <View style={styles.target}>
      <View style={{ marginRight: 12 }}>
        <Image
          source={{
            uri:
              'https://i.pinimg.com/originals/5d/42/c9/5d42c92e565fe88644caf6a4b2bfa30a.jpg',
          }}
          style={{
            width: 40,
            height: 40,
            borderRadius: 40,
          }}
        />
      </View>
      <View style={{ flex: 1 }}>
        <Text style={{ fontSize: 13,color: 'black' }}>
          <Text style={{ fontWeight: 'bold',color: 'black' }}>Isabel Zambrano </Text>
          Actualizo su foto de portada.
        </Text>
        <Text style={{ fontSize: 11, color: 'gray' }}>
          3 de Marzo <Text> </Text>
          <FontAwesome5
            name={'circle'}
            size={8}
            color="gray"
            solid
          ></FontAwesome5>
          <Text> </Text>
          <FontAwesome5
            name={'globe-americas'}
            size={11}
            color="gray"
          ></FontAwesome5>
        </Text>
      </View>
      <View style={{ marginRight: 12 }}>
        <Text>
          <FontAwesome5
            name={'ellipsis-h'}
            size={20}
            color="gray"
          ></FontAwesome5>
        </Text>
      </View>
    </View>
    <View>
      <Image
        source={{
          uri:
            'https://previews.123rf.com/images/lighthunter/lighthunter1403/lighthunter140300050/31324708-feliz-grupo-de-personas-familia-y-el-m%C3%A9dico-que-muestra-thumbsup-para-los-servicios-de-alta-calidad-en.jpg',
        }}
        style={{
          height: 200,
        }}
      />
    </View>
  </View>
)

const styles = StyleSheet.create({
  imagePerfil: {
    height: 50,
    width: 50,
    borderRadius: 50,
  },
  target: {
    margin: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
})

export default Car
