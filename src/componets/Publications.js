import React from 'react'
import { Text, View, StyleSheet } from 'react-native'
import Publication from './Publication'
import Fotos from './Fotos'

const Publications = props => (
  <View>
    <View style={styles.lineStyle} />
    <View style={styles.publications}>
      <Text style={styles.text}>Publicaciones</Text>
    </View>
    <View style={styles.lineStyle} />
    <View style={styles.public}>
      <View style={{ width: 118, margin: 5 }}>
        <Publication />
      </View>
      <View style={{ width: 118, margin: 5 }}>
        <Fotos />
      </View>
      <View style={{ width: 118, margin: 5 }}>
        <Fotos />
      </View>
    </View>
    <View style={styles.lineStyle} />
  </View>
)

const styles = StyleSheet.create({
  lineStyle: {
    borderWidth: 2,
    borderColor: '#eff0f1',
  },
  publications: {
    height: 50,
  },
  text: {
    padding: 15,
    fontWeight: 'bold',
    fontSize: 18,
    color: 'black'
  },
  public: {
    flexDirection: 'row',
    marginLeft: 10,
  },
})

export default Publications
