import React from 'react'
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native'
import Friend from './Freiend'

const Friends = props => (
  <View>
    <View style={{ marginLeft: 20 }}>
      <Text style={{ fontSize: 20, fontWeight: 'bold',color: 'black' }}>Amigos</Text>
      <Text style={{ color: 'gray', fontSize: 15 }}>4,794 Amigos</Text>
    </View>
    <View style={styles.card}>
      <Friend />
      <Friend />
      <Friend />
      <Friend />
      <Friend />
      <Friend />
    </View>
    <TouchableOpacity
      style={{ backgroundColor: '#dbdbdb', margin: 15, borderRadius: 7 }}
    >
      <Text
        style={{
          textAlign: 'center',
          color: 'black',
          fontSize: 15,
          paddingTop: 10,
          height: 40,
          fontWeight: 'bold',
        }}
      >
        Ver todos los Amigos
      </Text>
    </TouchableOpacity>
  </View>
)

const styles = StyleSheet.create({
  container: {
    margin: 15,
  },
  info: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  card: {
    flex: 1,
    flexWrap: 'wrap',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
})

export default Friends
