import React from 'react'
import { Text, View, StyleSheet, ImageBackground } from 'react-native'

const Publication = props => (
  <View>
    <ImageBackground
      source={{
        uri:
          'https://www.lifeder.com/wp-content/uploads/2015/10/calidad-de-vida.jpg',
      }}
      style={{ height: 118 }}
    >
      <View style={{ marginTop: 90, marginLeft: 15 }}>
        <Text style={styles.text}>Fotos</Text>
      </View>
    </ImageBackground>
  </View>
)

const styles = StyleSheet.create({
  lineStyle: {
    borderWidth: 0.5,
    borderColor: '#eff0f1',
  },
  text: {
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 16
  },
})

export default Publication
