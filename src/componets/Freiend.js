import React from "react";
import { Text, View, StyleSheet, ImageBackground } from "react-native";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";

const Friend = props => (
  <View style={styles.box}>
    <ImageBackground
      imageStyle={{ borderRadius: 7 }}
      source={{
        uri:
          "https://as.com/betech/imagenes/2016/08/30/portada/1472508577_544668_1472508642_noticia_normal.jpg"
      }}
      style={{ height: 100 }}
    >
      <View style={{ flexDirection: "row-reverse" }}>
        <FontAwesome5
          style={{
            backgroundColor: "#514d4d",
            borderRadius: 30,
            padding: 8,
            opacity: 0.5
          }}
          name={"user-plus"}
          size={15}
          color="white"
        />
      </View>
    </ImageBackground>
    <View>
      <Text style={{ fontSize: 12,color: 'black' }}>Mariel Arteaga</Text>
      <Text style={{ fontSize: 10, color: "gray" }}>10 amigos en comun</Text>
    </View>
  </View>
);

const styles = StyleSheet.create({
  box: {
    marginTop: 15,
    flexBasis: 90,
    height: 130,
    margin: 5
  }
});

export default Friend;
