import React from 'react'
import { Text, View, StyleSheet, ImageBackground } from 'react-native'

const Fotos = props => (
  <View style={styles.container}>
    <View style={{ marginTop: 90, marginLeft: 15 }}>
      <Text style={styles.text}>Acontecimientos...</Text>
    </View>
  </View>
)

const styles = StyleSheet.create({
  container: {
    height: 118,
    width: 118,
    backgroundColor: '#5abba9',
  },
  text: {
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 11,
  },
})

export default Fotos
