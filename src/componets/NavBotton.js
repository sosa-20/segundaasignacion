import React from 'react'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import { Text, View, StyleSheet, TextInput } from 'react-native'

const NavBotton = props => (
  <View style={styles.container}>
    <View style={styles.icon}>
      <Text>
        <FontAwesome5
          name={'window-maximize'}
          size={20}
          color="blue"
        ></FontAwesome5>
      </Text>
    </View>
    <View style={styles.icon}>
      <Text>
        <FontAwesome5
          name={'caret-square-right'}
          size={20}
          color="gray"
        ></FontAwesome5>
      </Text>
    </View>
    <View style={styles.icon}>
      <Text>
        <FontAwesome5 name={'store'} size={20} color="gray"></FontAwesome5>
      </Text>
    </View>
    <View style={styles.icon}>
      <Text>
        <FontAwesome5
          name={'user-circle'}
          size={25}
          color="gray"
        ></FontAwesome5>
      </Text>
    </View>
    <View style={styles.icon}>
      <Text>
        <FontAwesome5 name={'bell'} size={20} color="gray"></FontAwesome5>
      </Text>
    </View>
    <View style={styles.icon}>
      <Text>
        <FontAwesome5 name={'bars'} size={20} color="gray"></FontAwesome5>
      </Text>
    </View>
  </View>
)

const styles = StyleSheet.create({
  container: {
    margin: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  icon: {
    height: 20,
    width: 50,
    alignItems: 'center',
  },
})

export default NavBotton
