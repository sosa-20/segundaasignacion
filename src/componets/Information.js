import React from 'react'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import { Text, View, StyleSheet } from 'react-native'

const Information = props => (
  <View style={styles.container}>
    <View style={styles.target}>
      <View style={{ marginRight: 12 }}>
        <Text>
          <FontAwesome5
            name={'briefcase'}
            size={20}
            color="gray"
          ></FontAwesome5>
        </Text>
      </View>
      <View>
        <Text style={styles.info}>
          Presentadora deportes y noticias en{' '}
          <Text style={{ fontWeight: 'bold' }}>Televicentro HN</Text>
        </Text>
      </View>
    </View>
    <View style={styles.target}>
      <View style={{ marginRight: 12 }}>
        <Text>
          <FontAwesome5
            name={'graduation-cap'}
            size={20}
            color="gray"
          ></FontAwesome5>
        </Text>
      </View>
      <View>
        <Text style={styles.info}>
          Estudio en{' '}
          <Text style={{ fontWeight: 'bold' }}>
            UMH-Universidad Metropolitana de Honduras
          </Text>
        </Text>
      </View>
    </View>
    <View style={styles.target}>
      <View style={{ marginRight: 12 }}>
        <Text>
          <FontAwesome5 name={'home'} size={20} color="gray"></FontAwesome5>
        </Text>
      </View>
      <View>
        <Text style={styles.info}>
          Vive en<Text style={{ fontWeight: 'bold' }}>Tegucigalpa</Text>
        </Text>
      </View>
    </View>
    <View style={styles.target}>
      <View style={{ marginRight: 12 }}>
        <Text>
          <FontAwesome5
            name={'heart'}
            size={20}
            color="gray"
            solid
          ></FontAwesome5>
        </Text>
      </View>
      <View>
        <Text style={styles.info}>
          En una relacion
          <Text style={{ fontWeight: 'bold' }}></Text>
        </Text>
      </View>
    </View>
    <View style={styles.target}>
      <View style={{ marginRight: 12 }}>
        <Text>
          <FontAwesome5
            name={'ellipsis-h'}
            size={20}
            color="gray"
          ></FontAwesome5>
        </Text>
      </View>
      <View>
        <Text style={styles.info}>
          Ver la informacion de Isabel
          <Text style={{ fontWeight: 'bold' }}></Text>
        </Text>
      </View>
    </View>
  </View>
)

const styles = StyleSheet.create({
  container: {
    margin: 15,
  },
  target: {
    marginTop: 10,
    flexDirection: 'row',
  },
  info: {
    fontSize: 17,
    color: 'black'
  },
})

export default Information
